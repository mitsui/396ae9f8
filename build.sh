#!/bin/bash
set -eo pipefail
export HOME=`pwd`
MAKE_THREADS=${MAKE_THREADS:-$(expr `nproc` + 1)}
OPENWRT_VERSION=${OPENWRT_VERSION:-19.07.0}
OPENWRT_CONFIG_FILE_VER=$OPENWRT_VERSION

pack_and_upload() {
  echo 'Uploading artifacts...'
  cd $HOME/firmware
  local build_time=`date +"%Y%m%d%H%M%S"`
  tar cJf "$HOME/Openwrt.tar.xz" *
  curl -T "$HOME/Openwrt.tar.xz" "https://transfer.sh/Openwrt-$build_time.tar.xz"
  echo
  if [ -n "$TERACLOUD_AUTH" -a -n "$TERACLOUD_HOST" ]; then
    curl -u "$TERACLOUD_AUTH" -T "$HOME/Openwrt.tar.xz" "https://$TERACLOUD_HOST/dav/artifacts/Openwrt-$build_time.tar.xz" >/dev/null
    echo
  fi
}

mkdir -p $HOME/firmware || true

git clone --single-branch -b $OPENWRT_VERSION https://git.openwrt.org/openwrt/openwrt.git
cd openwrt
OPENWRT_VERSION=$(git describe --tags || git rev-parse --short HEAD) || true


echo 'Updating feeds...'
./scripts/feeds update -a > /dev/null 2>&1
echo 'Install packages...'
./scripts/feeds install -a > /dev/null 2>&1

for d in $BUILD_DEVICES; do
  make clean >/dev/null 2>&1 || true
  echo '--------start building--------'
  echo "Build openwrt $OPENWRT_VERSION for $d."
  if [ -f $HOME/build_configs/$d-$OPENWRT_CONFIG_FILE_VER.config ]; then
    cp $HOME/build_configs/$d-$OPENWRT_CONFIG_FILE_VER.config ./.config
  elif [ -f $HOME/build_configs/$d-default.config ]; then
    echo 'Version specified config file not found, using device default config insteaded.'
    cp $HOME/build_configs/$d-default.config ./.config
  else
    echo "Config file for $d not found, build failed."
    exit 1
  fi
  echo '--------config target--------'
  make defconfig
  echo '--------make download--------'
  make -j5 download
  echo '--------make firmware--------'
  make -j$MAKE_THREADS V=s
  echo '--------move artifacts--------'
  mv bin/targets/*/*/*.bin $HOME/firmware
done

cd $HOME/firmware
if [ "$(ls -A .)" ]; then
  echo "build succeed:"
  ls -hl
  [ -n "$BUILD_NOHOP_MODE" ] && touch $HOME/build_success || true
  exit 0
fi
exit 1
